/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__blocks_header_header__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__blocks_header_header___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__blocks_header_header__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__blocks_footer_footer__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__blocks_footer_footer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__blocks_footer_footer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__blocks_menu_menu__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__blocks_menu_menu___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__blocks_menu_menu__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__blocks_intro_intro__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__blocks_intro_intro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__blocks_intro_intro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__blocks_mainContent_mainContent__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__blocks_mainContent_mainContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__blocks_mainContent_mainContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__blocks_abaContent_abaContent__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__blocks_abaContent_abaContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__blocks_abaContent_abaContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__blocks_clContent_clContent__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__blocks_clContent_clContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__blocks_clContent_clContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__blocks_crContent_crContent__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__blocks_crContent_crContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__blocks_crContent_crContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__blocks_clSample_clSample__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__blocks_clSample_clSample___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__blocks_clSample_clSample__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__blocks_calendar_calendar__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__blocks_calendar_calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__blocks_calendar_calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__blocks_register_register__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__blocks_register_register___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__blocks_register_register__);












/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#header').headroom();
});
$('.header__icon').click(function () {
  console.log("burger");
  TweenMax.to('.header__menu', 1, {
    autoAlpha: 1,
    x: -336
  });
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {



/***/ }),
/* 3 */
/***/ (function(module, exports) {

$(document).ready(function () {});
$('.menu__close').click(function () {
  TweenMax.to('.header__menu', 1.5, {
    autoAlpha: 0,
    x: 336,
    ease: Power4.easeInOut
  });
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {



/***/ }),
/* 5 */
/***/ (function(module, exports) {

$(document).ready(function () {
  // console.log('mainContent')
  $('#learn').click(function () {
    $('.calendar').css('visibility', 'visible');
  });
  $('#reg').click(function () {
    $('.register').css('visibility', 'visible');
  });
});

/***/ }),
/* 6 */
/***/ (function(module, exports) {



/***/ }),
/* 7 */
/***/ (function(module, exports) {



/***/ }),
/* 8 */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#upload-btn').click(function () {
    $('#my_file').click();
  });
  $('#form').parsley();
});

/***/ }),
/* 9 */
/***/ (function(module, exports) {



/***/ }),
/* 10 */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#exit-btn').click(function () {
    $('.calendar').css('visibility', 'hidden');
  });
  $('#calendar').fullCalendar({
    header: {
      left: 'title',
      center: '',
      right: ''
    },
    defaultDate: '2019-01-12',
    navLinks: true,
    // can click day/week names to navigate views
    editable: true,
    eventLimit: true,
    // allow "more" link when too many events
    events: [{
      title: 'Help(7PM)',
      start: '2019-01-29T19:00:00',
      end: '2019-01-29T23:00:00',
      color: '#20BDBF'
    }]
  });
});

/***/ }),
/* 11 */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#cancel-btn').click(function () {
    $('.register').css('visibility', 'hidden');
  });
  $('#submit-btn').click(function () {
    $('.register').css('visibility', 'hidden');
  });
  $('#form').parsley();
});

/***/ })
/******/ ]);