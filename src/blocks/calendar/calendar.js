$(document).ready(function() {

  $('#exit-btn').click(function(){
    $('.calendar').css('visibility', 'hidden');
  })

  $('#calendar').fullCalendar({
    header: {
      left: 'title',
      center: '',
      right: ''
    },
    defaultDate: '2019-01-12',
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [
      {
        title: 'Help(7PM)',
        start: '2019-01-29T19:00:00',
        end: '2019-01-29T23:00:00',
        color: '#20BDBF'
      }
    ]
  });

});